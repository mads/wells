PROG = wells
CC = gcc 
CFLAGS = -Wall -lm
OBJ = wells.o
$(PROG): $(OBJ) -lm

wells.o: wells.c wells.h

clean:
	rm -f $(PROG) $(OBJ)

tar:
	tar -cvzf wells.tgz `hg st -c | awk '{print $$2}'` .hg


